from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Mandatory code template for Data Science Projects',
    author='Advanced Analytics MAPFRE SPAIN',
    license='',
)
